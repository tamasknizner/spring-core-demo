package spring.common.repository;

import java.util.List;

import spring.common.domain.User;

public interface UserRepository {

    int calculateNextId();

    void saveUser(User user);

    User getUserById(Integer id);

    List<User> findAllUsers();
}

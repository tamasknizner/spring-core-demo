package spring.common.repository;

import java.util.List;

import org.springframework.stereotype.Component;

import spring.common.domain.User;

@Component
public class NoOpUserRepository implements UserRepository {
    @Override
    public int calculateNextId() {
        throw new RuntimeException();
    }

    @Override
    public void saveUser(User user) {
        throw new RuntimeException();
    }

    @Override
    public User getUserById(Integer id) {
        throw new RuntimeException();
    }

    @Override
    public List<User> findAllUsers() {
        throw new RuntimeException();
    }
}

package spring.common.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Component;

import spring.common.domain.User;

@Component("userRepository")
public class DefaultUserRepository implements UserRepository {

    private final Map<Integer, User> users = new ConcurrentHashMap<>();

    @Override
    public int calculateNextId() {
        return users.size() + 1;
    }

    @Override
    public void saveUser(User user) {
        if (user.getId() != null) {
            users.put(user.getId(), user);
        } else {
            User saveUser = new User(calculateNextId(), user.getName());
            users.put(saveUser.getId(), saveUser);
        }
    }

    @Override
    public User getUserById(Integer id) {
        return users.get(id);
    }

    @Override
    public List<User> findAllUsers() {
        return new ArrayList<>(users.values());
    }
}

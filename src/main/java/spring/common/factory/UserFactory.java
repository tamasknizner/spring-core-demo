package spring.common.factory;

import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Component;

import spring.common.domain.User;
import spring.common.repository.UserRepository;

@Component
public class UserFactory {

    private final List<String> availableNames;
    private final UserRepository userRepository;

    public UserFactory(List<String> availableNames, UserRepository userRepository) {
        this.availableNames = availableNames;
        this.userRepository = userRepository;
    }

    public User create(Integer id, String name) {
        return new User(id, name);
    }

    public User createNew() {
        return create(userRepository.calculateNextId(), getRandomName());
    }

    private String getRandomName() {
        return availableNames.get(new Random().nextInt(availableNames.size()));
    }
}

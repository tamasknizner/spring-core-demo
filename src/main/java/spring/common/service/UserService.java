package spring.common.service;

import java.util.List;

import spring.common.domain.User;

public interface UserService {

    List<User> createUsers(int numberOfUsers);

}

package spring.common.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import spring.common.domain.User;
import spring.common.factory.UserFactory;
import spring.common.repository.UserRepository;

// UserService userService = new DefaultUserService(new UserRepository(), new UserFactory()) -> ilyet nem kell csinálnom
@Service("userService")
public class DefaultUserService implements UserService {

    private final UserRepository userRepository;
    private final UserFactory userFactory;
    private final String endpoint;

    public DefaultUserService(UserRepository userRepository, UserFactory userFactory, @Value("${service.endpoint}") String endpoint) {
        this.userRepository = userRepository;
        this.userFactory = userFactory;
        this.endpoint = endpoint;
        System.out.println(this.endpoint);
    }

    @Override
    public List<User> createUsers(int numberOfUsers) {
        for (int i = 0; i < numberOfUsers; i++) {
            User user = userFactory.createNew();
            userRepository.saveUser(user);
        }
        return userRepository.findAllUsers();
    }
}

package spring.common.domain;

import java.util.Objects;

import org.springframework.beans.factory.InitializingBean;

public class User implements InitializingBean {
    private final Integer id;
    private final String name;

    public User(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) && Objects.equals(name, user.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public void onDestroy() {
        System.out.println("onDestroy " + this);
    }

    public void onInit() {
        System.out.println("onInit " + this);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("properties are initialized");
    }
}

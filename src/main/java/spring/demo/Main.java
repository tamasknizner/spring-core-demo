package spring.demo;

import java.lang.reflect.Field;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.util.ReflectionUtils;

import spring.common.repository.DefaultUserRepository;
import spring.common.repository.UserRepository;
import spring.common.service.DefaultUserService;
import spring.config.AppConfig;
import spring.common.domain.User;
import spring.common.service.UserService;

/*
Spring
- Inversion of control -> Dependency injection
 -> IOC Container -> Spring (managed) Bean-ek
    -> Application context -> ebben vannak benne a bean-ek
        -> spring bean -> proxy
    -> configuration:
        - xml (BookingApp)
            pro: nem invazív
            con: úgy néz ki mint egy rakás szar
        - java
            - @Configuration osztályok
            pro: nem xml, jobban olvasható
            con: nem javában kéne configurálni
        - annotation: @Component, @Service, @Controller, @Repository
            - component scan
            pro: minimális config
            con: invazív
            - yml / properties fájlból értéket injektálni -> @Value("${...}") -> @PropertySource
    -> injection types
        - constructor (good)
        - setter (not good)
        - field injection (meh)
    -> bean scopes (in spring core)
        - singleton: 1 db van belőle
        - prototype: akárhányszor kérek egy olyan az application context-től, kapok egy újat
 */

public class Main {

    public static void main(String[] args) {
        ApplicationContext applicationContext = getApplicationContext();

        UserService userService = applicationContext.getBean("userService", UserService.class);
        List<User> users = userService.createUsers(3);
        System.out.println(users);

//        // prototype scope
        User user1 = applicationContext.getBean("theUser", User.class);
        User user2 = applicationContext.getBean("theUser", User.class);
        User user3 = applicationContext.getBean("theUser", User.class);
        System.out.println(user1);
        System.out.println(user2);
        System.out.println(user3);
    }

    private static ApplicationContext getApplicationContext() {
//        return new ClassPathXmlApplicationContext("beans.xml");
        return new AnnotationConfigApplicationContext(AppConfig.class);
    }
}

package spring.config;

import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;

import spring.common.domain.User;
import spring.common.factory.UserFactory;

@Configuration
@ComponentScan(basePackages = {"spring.common"})
@PropertySource("classpath:application.yml")
public class AppConfig {

//    @Value("${service.endpoint}")
//    private String endpoint;
//
//    @Bean
//    @Qualifier("userService")
//    public UserService defaultUserService(UserFactory userFactory) {
//        return new DefaultUserService(userRepository(), userFactory, endpoint);
//    }
//
//    @Bean
//    public UserFactory userFactory(UserRepository userRepository) {
//        return new UserFactory(availableNames(), userRepository);
//    }
//
//    @Bean
//    public UserRepository userRepository() {
//        return new DefaultUserRepository();
//    }

    @Bean
    public List<String> availableNames() {
        return List.of("Adam", "Steve", "Kate", "George");
    }

    @Bean(destroyMethod = "onDestroy", initMethod = "onInit")
    @Scope("prototype")
    public User theUser(UserFactory userFactory) {
        return userFactory.createNew();
    }
}
